#require 'game'
require 'spec_helper'

describe Game do
	let(:p1) {double :player, name: "Bob"}
	let(:p2) {double :player, name: "Karen"}
	let(:master) {GameMaster.new(p1,p2)}
	#let(:board){ThreeByThreeBoard.new()}
	#let(:game){Game.new(p1,p2,board)}
    

	it 'starts empty' do
        expect(master.board.empty).to eq true 
		#expect(game.grid.count).to eq 9
	end

	it "has two players" do 
		expect(master.game.players.count).to eq 2
	end

	it "can take turns from a player" do 
		master.game.go(1)
		#expect(game.grid).to eq(["x",nil,nil,nil,nil,nil,nil,nil,nil])
        expect(master.board.is_this_board_readout(["x",nil,nil,nil,nil,nil,nil,nil,nil])).to eq true
	end

	it "should know whos turn it is starting with player 1" do
	 expect(master.game.turn).to eq(p1)
	end

	it "should switch turns after player1 has had a go" do
		master.game.go(1)
		expect(master.game.turn).to eq(p2) 
	end

	it 'should plot an "o" for player 2' do 
		master.game.go(1)
		master.game.go(2)
		#expect(game.grid[1]).to eq("o")
        expect(master.board.has_board_entry(1,"o")).to eq true
	end

	it "should know if there is a winner" do
		_win_for_player_one
		expect(master.game.winner).to eq(p1)
	end

	it "raises error if someones won" do
	_win_for_player_one
	expect(lambda{master.game.go(7)}).to raise_error "Too late, Bob won" 
	end

	it "should expect a winner" do
	_func_for_find_bug
	expect(master.game.winner).to eq(p1) 
	end

	def _win_for_player_one
		[1,4,2,5,3].each{|plot|master.game.go(plot)}
	end


	def _func_for_find_bug
		[2,1,5,7,8].each{|plot|master.game.go(plot)}
	end

	it 'starts empty always nil' do
	
		@threebythreeboard = double('threebythreeboard')

		ThreeByThreeBoard.any_instance.stub(:empty).and_return(true)
		o = ThreeByThreeBoard.new

		expect(ThreeByThreeBoard).to receive(:new).and_return(o)	
	
		master.game.go(1)
		expect(master.board.empty).to eq true 		
	end
	
	it 'dummy return for board X should win' do
	
		@threebythreeboard = double('threebythreeboard')

		ThreeByThreeBoard.any_instance.stub(:board).and_return(["x","x","x","0","","x","o","x",""])
		o = ThreeByThreeBoard.new

		expect(ThreeByThreeBoard).to receive(:new).and_return(o)	
	
		expect(master.game.winner).to eq(p1) 		
	end

	it 'dummy return for board O should win' do
	
		@threebythreeboard = double('threebythreeboard')

		ThreeByThreeBoard.any_instance.stub(:board).and_return(["o","0","0","0","o","o","o","o","o"])
		o = ThreeByThreeBoard.new

		expect(ThreeByThreeBoard).to receive(:new).and_return(o)	
	
		expect(master.game.winner).to eq(p2) 		
	end

	it 'no winner in case of some other value' do
	
		@threebythreeboard = double('threebythreeboard')

		ThreeByThreeBoard.any_instance.stub(:board).and_return(["l","l","l","l","l","l","l","l","l"])
		o = ThreeByThreeBoard.new

		expect(ThreeByThreeBoard).to receive(:new).and_return(o)	
	
		expect(master.game.winner).to eq(p2) 		
	end

end

