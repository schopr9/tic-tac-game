class Game
	#attr_accessor :grid
	attr_reader :players, :player2, :player1,:turn
	WINNING_COMBOS = [[0,1,2],[3,4,5],[6,7,8],[0,3,6],[1,4,7],[2,5,8],[2,4,6],[0,4,8]]


	def initialize(player1, player2,board)
		#@grid = Array.new(9)
		@players = [player1,player2]
		@player1 = player1
		@player2 = player2
		@turn = player1
		p @board = board
	end

	#private attr_accessor :board

	def go(position)
		raise "Too late, #{winner.name} won" if winner
		mark = @turn == player1  ? "x" : "o"
		@turn == player1 ? @turn = player2 : @turn = player1
		#grid[position -1] = mark
		@board.update_position(position-1,mark)
	end

	def winner
		grid = @board.board
		winner = WINNING_COMBOS.select do |combo|			
			grid[combo[0]] == grid[combo[1]] && grid[combo[1]] == grid[combo[2]] && grid[combo[0]]
		end
		grid[winner.first.first] == "x" ? player1 : player2 if winner.any?		
	end


end

class ThreeByThreeBoard
    
	attr_reader :board

	def initialize()	
		@board = Array.new(9)

	end

    def empty
        check1 = @board.compact
		return check1.empty?
	end	

	def is_this_board_readout(input_grid)
		return @board == input_grid
	end	

	def has_board_entry(loc,val)
		return @board[loc] == val
	end	

	def update_position(loc,val)

		@board[loc] = val
	end

end

class GameMaster
	attr_accessor :board, :game
	
	def initialize(player1,player2)
		@board = ThreeByThreeBoard.new()
		@game = Game.new(player1,player2,board)		  
	end
	

end
